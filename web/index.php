<?php
use Library\Database\PdoAdapter,
    Model\Mapper\UserMapper,
    Service\UserService,
    Service\Serializer,
    Service\JsonEncoder;

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\ClassLoader\UniversalClassLoader;
$autoloader = new UniversalClassLoader();
$autoloader->register();

$adapter = new PdoAdapter('mysql:dbname=my_demo_examples', 'demo', 'demo');

$userService = new UserService(new UserMapper($adapter));

$userService->setEncoder(new JsonEncoder);
print_r($userService->fetchAllEncoded());
print_r($userService->fetchByIdEncoded(1));

$userService->setEncoder(new Serializer());
print_r($userService->fetchAllEncoded(array("ranking" => "high")));
print_r($userService->fetchByIdEncoded(1));