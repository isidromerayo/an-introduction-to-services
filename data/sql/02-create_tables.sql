CREATE TABLE posts (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) DEFAULT NULL,
  content TEXT,
  PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE users (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) DEFAULT NULL,
  email VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE comments (
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  content TEXT,
  user_id INTEGER DEFAULT NULL,
  post_id INTEGER DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX (user_id),
  INDEX (post_id)
) ENGINE=INNODB;

-- Error code 1005, SQL state HY000
-- ALTER TABLE comments ADD  FOREIGN KEY (user_id) REFERENCES users(id);
-- ALTER TABLE comments ADD  FOREIGN KEY (post_id) REFERENCES posts(id);